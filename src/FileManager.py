import os
import sys

class FileManager: 
    
    def readFile(self,fileName):
	try:
		rawStr = open(fileName,"r").readlines();
		return rawStr
	except IOError:
		print "\n\r Can't read File "+fileName+" \n\r ";
		sys.exit (0)

    def writeFile(self,fileName,contents):
        return open(fileName,"w").writelines(contents);
    
    def deleteFile(self,fileName):
        if(self.fileExists(fileName)):
            return os.remove(fileName)
            
    def fileExists(self,fileName):
        return os.path.exists(fileName);
    
    def getHandle(self,fileName,flag):
        fh = open(fileName,flag)
        return fh;
        

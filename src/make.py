from distutils.core import setup
import py2exe, sys, os

sys.argv.append('py2exe')

options = {
    "bundle_files": 1,
    "compressed": 1,
    "dll_excludes": ["w9xpopen.exe"]
    }
    
setup(
	options = {'py2exe': options},
	console=['replace.py'],
	zipfile = None,
)
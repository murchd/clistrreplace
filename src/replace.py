import string;
import os;
import FileManager;
import sys;

def main(*argv):
	try:
		if(sys.argv[1] == "--help"):
			print "\r\n usage:\r\n";
			print "\treplace 'oldString' 'newString' 'fileName' ";
			sys.exit (0)
		
		fm = FileManager.FileManager();
		contents = fm.readFile(sys.argv[3]);
		output = [];

		for s in contents:	
			s = s.replace(sys.argv[1],sys.argv[2]);
			output.append(s);	

		fm.writeFile(sys.argv[3],output);
	except IndexError:
		print " \r\n Invalid Argument(s) \r\n \tUse '--help' for help.";

if __name__ == '__main__':
	main()